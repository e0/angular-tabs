angular.module('fs').directive('tabs', function () {
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			tabsTitle: '@'
		},
		controller: ['$scope', function ($scope) {
			var panes = $scope.panes = [];

			$scope.select = function (pane, index) {
				if (!!!index)
					index = 0;
				angular.forEach(panes, function (pane) {
					pane.selected = false;
				});
				pane.selected = true;
				$scope.$parent.selectedTabIndex = index;
			};

			this.addPane = function (pane) {
				if (panes.length == 0) $scope.select(pane);
				panes.push(pane);
			};
		}],
		templateUrl: "Assets/Templates/Directives/tabs.html",
		replace: true
	};
}).directive('pane', function () {
    return {
    	require: '^tabs',
    	restrict: 'E',
    	transclude: true,
    	scope: { title: '@' },
    	link: function (scope, element, attrs, tabsCtrl) {
    		tabsCtrl.addPane(scope);
    	},
    	template:
            '<div class="tab-pane" ng-class="{active: selected}" ng-transclude>' +
                '</div>',
    	replace: true
    };
});